﻿/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 //ZD 100886
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Xml;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Google.Apis.Util;
using Google.Apis.Authentication.OAuth2;
using Google.Apis.Authentication.OAuth2.DotNetOpenAuth;
using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OAuth2;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Requests;
using Google.Apis.Services;
using Google.Apis.Util.Store;

//using Google.Apis.Util;
//using Google.Apis.Authentication.OAuth2;
using Google.Apis.Auth.OAuth2;
//using DotNetOpenAuth.Messaging;
//using DotNetOpenAuth.OAuth2;
//using Google.Apis.Calendar.v3;
//using Google.Apis.Calendar.v3.Data;
using System.Threading;
//using Google.Apis.Services;
//using Google.Apis.Authentication.OAuth2.DotNetOpenAuth;
using System.Text;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
//using Google.Apis.Util.Store;
//using Google.Apis;
using System.Security.Cryptography.X509Certificates;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Web;


namespace ns_MyVRM
{
    public partial class GoogleAuth : System.Web.UI.Page
    {
        #region Declaration

        #region myVRM Declartion

        ns_Logger.Logger _log;
        private myVRMNet.NETFunctions _obj;
        string _outXML = "";
        StringBuilder _inXML = null;

        #endregion


        #region Google Calendar
        private CalendarService _service;
        private OAuth2Authenticator<WebServerClient> _authenticator;
        private IAuthorizationState _state;
        private Google.Apis.Calendar.v3.Data.Channel _channel = null;
        private Google.Apis.Calendar.v3.Data.Channel _chresp = null;
        CalendarService calendarConnection;
        private IAuthorizationState AuthState
        {
            get
            {
                return _state ?? HttpContext.Current.Session["AUTH_STATE"] as IAuthorizationState;
            }
        }
        #endregion

        #endregion

        #region Constructor
        public GoogleAuth()
        {
            _log = new ns_Logger.Logger();
            _obj = new myVRMNet.NETFunctions();
        }
        # endregion

        #region Page_Load
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                /*               
                if (HttpContext.Current.Session["SERVICE_OBJECT"] != null)
                {
                    _service = (CalendarService)HttpContext.Current.Session["SERVICE_OBJECT"];
                    //HttpContext.Current.Response.Redirect("~/" + HttpContext.Current.Session["language"].ToString() + "/SettingSelect2.aspx?c=GH");
                    HttpContext.Current.Response.Redirect("~/en/SettingSelect2.aspx?c=GH"); //ZD 101344
                }
                else*/
                {

                    connectCalendar();
                   //     UserCredential credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                   //        secrets,
                   //        new string[]
                   //         { 
                   //                 CalendarService.Scope.Calendar
                   //         },
                   //       HttpContext.Current.Session["userEmail"].ToString(),
                   //        CancellationToken.None)
                   //.Result;

                   //     var initializer = new BaseClientService.Initializer();
                   //     initializer.HttpClientInitializer = credential;
                   //     initializer.ApplicationName = "MyProject";
                   //     calendarConnection = new CalendarService(initializer);

                   //     //_service = new CalendarService(new BaseClientService.Initializer() { Authenticator = _authenticator = CreateAuthenticator() });
                   //     _service = new CalendarService(initializer);

                   //     //_authenticator.LoadAccessToken();
                }
            }
            catch (System.Threading.ThreadAbortException)
            { }
            catch (Exception ex)
            {
                _log.Trace("" + ex.StackTrace);
            }
        }
        #endregion

        #region new Google Calender
        /// <summary>
        /// connectCalendar
        /// </summary>
        /// <returns></returns>
        public bool connectCalendar()
        {
            ClientSecrets secrets = new ClientSecrets
            {
                ClientId = _obj.GetDecryptText(HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString(), HttpContext.Current.Session["GoogleClientID"].ToString()), //"194415732158-ichuumlpsmi0in3085tu63kp9g6i98i4.apps.googleusercontent.com",
                ClientSecret = _obj.GetDecryptText(HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString(), HttpContext.Current.Session["GoogleSecretID"].ToString())//"k2WV0HUMtHVraUYwRe_U0tvd"
            };

            try
            {

                /*var token = new TokenResponse {}; 

                var credentials = new UserCredential(new GoogleAuthorizationCodeFlow(
                new GoogleAuthorizationCodeFlow.Initializer
                {
                    ClientSecrets = secrets,
                    Scopes = new[] { CalendarService.Scope.Calendar },

                }),
               HttpContext.Current.Session["userEmail"].ToString(),
                token);*/

                GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow(
                new GoogleAuthorizationCodeFlow.Initializer
                {
                    DataStore = new FileDataStore("Calendar.ASP.NET.Sample.Store"),
                    ClientSecrets = secrets,
                    Scopes = new[] { CalendarService.Scope.Calendar },

                });

                var uri = Request.Url.ToString();
                var code = Request["code"];
                if (code != null)
                {
                    var token = flow.ExchangeCodeForTokenAsync(HttpContext.Current.Session["userEmail"].ToString(), code,
                        uri.Substring(0, uri.IndexOf("?")), CancellationToken.None).Result;

                    // Extract the right state.
                    var oauthState = AuthWebUtility.ExtracRedirectFromState(
                        flow.DataStore, HttpContext.Current.Session["userEmail"].ToString(), Request["state"]).Result;



                   var result = new AuthorizationCodeWebApp(flow, uri, uri).AuthorizeAsync(HttpContext.Current.Session["userEmail"].ToString(),
                      CancellationToken.None).Result;
                    calendarConnection = new CalendarService(new BaseClientService.Initializer
                    {
                        ApplicationName = "myVRM Calendar Sync",
                        HttpClientInitializer = result.Credential
                    });

                    

                    var calendars = calendarConnection.CalendarList.List().Execute().Items;
                    HttpContext.Current.Session.Add("SERVICE_OBJECT", calendarConnection);
                    
                    //if (DateTime.Parse(HttpContext.Current.Session["GoogleChanelExpiration"].ToString()) < DateTime.UtcNow)
                    {
                        try //ZD 102980
                        {
                            _channel = new Google.Apis.Calendar.v3.Data.Channel();
                            _channel.Id = Guid.NewGuid().ToString();

                            _channel.Type = "web_hook";
                            _channel.Address = HttpContext.Current.Session["GoogleChannelAddress"].ToString() + @"/en/GoogleReceiver.aspx";
                            _channel.Token = HttpContext.Current.Session["userID"].ToString();

                            _chresp = new Google.Apis.Calendar.v3.Data.Channel();
                            _chresp = calendarConnection.Events.Watch(_channel, HttpContext.Current.Session["userEmail"].ToString()).Execute();

                        }
                        catch (Exception ex1)
                        {

                            _log.Trace(ex1.Message + " source " + ex1.StackTrace);
                        }



                    }
                    setusertoken(result.Credential, _chresp);

                    HttpContext.Current.Response.Redirect("~/en/SettingSelect2.aspx?c=GH");                   
                    
                }
                else
                {
                    var result = new AuthorizationCodeWebApp(flow, uri, uri).AuthorizeAsync(HttpContext.Current.Session["userEmail"].ToString(),
                        CancellationToken.None).Result;
                    if (result.RedirectUri != null)
                    {
                        // Redirect the user to the authorization server.
                        HttpContext.Current.Response.Redirect(result.RedirectUri);
                    }
                    else
                    {
                        // The data store contains the user credential, so the user has been already authenticated.
                        result = new AuthorizationCodeWebApp(flow, uri, uri).AuthorizeAsync(HttpContext.Current.Session["userEmail"].ToString(),
                     CancellationToken.None).Result;
                        calendarConnection = new CalendarService(new BaseClientService.Initializer
                        {
                            ApplicationName = "myVRM Calendar Sync",
                            HttpClientInitializer = result.Credential
                        });

                        //setusertoken(result.Credential);

                        var calendars = calendarConnection.CalendarList.List().Execute().Items;
                        HttpContext.Current.Session.Add("SERVICE_OBJECT", calendarConnection);
                        
                       // if (DateTime.Parse(HttpContext.Current.Session["GoogleChanelExpiration"].ToString()) < DateTime.UtcNow)
                        {
                            try //ZD 102980
                            {
                                _channel = new Google.Apis.Calendar.v3.Data.Channel();
                                 _channel.Id = Guid.NewGuid().ToString();

                                _channel.Type = "web_hook";
                                _channel.Address = HttpContext.Current.Session["GoogleChannelAddress"].ToString() + @"/en/GoogleReceiver.aspx";
                                //_channel.Address = "https://demo1.myvrm.com/en/GoogleReceiver.aspx";
                                _channel.Token = HttpContext.Current.Session["userID"].ToString();

                                _chresp = new Google.Apis.Calendar.v3.Data.Channel();
                                _chresp = calendarConnection.Events.Watch(_channel, HttpContext.Current.Session["userEmail"].ToString()).Execute();

                            }
                            catch (Exception ex1)
                            {

                                _log.Trace(ex1.Message + " source " + ex1.StackTrace);
                            }
                        }

                        setusertoken(result.Credential, _chresp);
                        HttpContext.Current.Response.Redirect("~/en/SettingSelect2.aspx?c=GH");
                       
                       
                    }
                }

                //UserCredential credentials = GoogleWebAuthorizationBroker.AuthorizeAsync(
                //           secrets,
                //           new string[]
                //            { 
                //                    CalendarService.Scope.Calendar
                //            },
                //          HttpContext.Current.Session["userEmail"].ToString(),
                //           CancellationToken.None)
                //   .Result;


               /* var initializer = new BaseClientService.Initializer();
                initializer.HttpClientInitializer = credentials;
                initializer.ApplicationName = "myVRM Calendar Sync";                
                calendarConnection = new CalendarService(initializer);

                var calendars = calendarConnection.CalendarList.List().Execute().Items;*/

            }
            catch (System.Threading.ThreadAbortException)
            { }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;
        }
        #endregion

        #region CreateAuthenticator
        /// <summary>
        /// CreateAuthenticator
        /// </summary>
        /// <returns></returns>
        private OAuth2Authenticator<WebServerClient> CreateAuthenticator()
        {

            try
            {

                // Register the authenticator.
                var provider = new WebServerClient(GoogleAuthenticationServer.Description, _obj.GetDecryptText(HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString(), HttpContext.Current.Session["GoogleClientID"].ToString()), _obj.GetDecryptText(HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString(), HttpContext.Current.Session["GoogleSecretID"].ToString()));

                var authenticator =
                    new OAuth2Authenticator<WebServerClient>(provider, GetAuthorization) { NoCaching = true };
                return authenticator;
            }
            catch (Exception e)
            {
                _log.Trace("" + e.StackTrace);
                return null;
            }
        }
        #endregion

        #region GetAuthorization
        /// <summary>
        /// GetAuthorization
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        private IAuthorizationState GetAuthorization(WebServerClient client)
        {
            string currentGoogleEmail = "";
            try
            {
                if (_obj == null)
                    _obj = new myVRMNet.NETFunctions();
                // If this user is already authenticated, then just return the auth state.

                IAuthorizationState state = AuthState;

                if (state != null)
                    return state;

                // Check if an authorization request already is in progress.
                state = client.ProcessUserAuthorization(new HttpRequestInfo(HttpContext.Current.Request));
                if (state == null)
                {
                    string scope = CalendarService.Scope.Calendar;
                    List<string> scopesl = new List<string>();
                    scopesl.Add(scope);
                    IEnumerable<string> scopes = scopesl.AsEnumerable();
                    OutgoingWebResponse response = client.PrepareRequestUserAuthorization(new[] { scope }, null);
                    if (string.IsNullOrEmpty((HttpContext.Current.Session["GoogleRefreshToken"].ToString())))
                        response.Headers["Location"] += "&access_type=offline&approval_prompt=force";
                    response.Send();

                }

                if (state != null && (!string.IsNullOrEmpty(state.AccessToken) || !string.IsNullOrEmpty(state.RefreshToken)))
                {
                    //Checking the authenticated email account
                    var accessToken = state.AccessToken;

                    var urlBuilder = new System.Text.StringBuilder();

                    urlBuilder.Append("https://");
                    urlBuilder.Append("www.googleapis.com");
                    urlBuilder.Append("/calendar/v3/users/me/calendarList");
                    urlBuilder.Append("?minAccessRole=writer");

                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(urlBuilder.ToString());
                    // Get the response.
                    httpWebRequest.CookieContainer = new CookieContainer();
                    httpWebRequest.Headers["Authorization"] =
                        string.Format("Bearer {0}", accessToken);

                    var response1 = (HttpWebResponse)httpWebRequest.GetResponse();
                    // Get the stream containing content returned by the server.
                    var dataStream = response1.GetResponseStream();
                    // Open the stream using a StreamReader for easy access.
                    var reader = new StreamReader(dataStream);
                    // Read the content. 
                    var jsonString = reader.ReadToEnd();
                    // Cleanup the streams and the response.
                    reader.Close();
                    dataStream.Close();
                    response1.Close();

                    XmlDocument doc = (XmlDocument)Newtonsoft.Json.JsonConvert.DeserializeXmlNode(jsonString, "Root");

                    if (doc.SelectSingleNode("Root/items/id") != null)
                        currentGoogleEmail = doc.SelectSingleNode("Root/items/id").InnerText;

                    if (currentGoogleEmail != HttpContext.Current.Session["userEmail"].ToString())
                    {
                        //HttpContext.Current.Response.Redirect("~/" + HttpContext.Current.Session["language"].ToString() + "/GenLogin.aspx?G=f");
                        HttpContext.Current.Response.Redirect("~/en/GenLogin.aspx?G=f");
                    }


                    HttpContext.Current.Session["AUTH_STATE"] = _state = state;
                    if (DateTime.Parse(HttpContext.Current.Session["GoogleChanelExpiration"].ToString()) < DateTime.UtcNow)
                    {
                        try //ZD 102980
                        {
                            _channel = new Google.Apis.Calendar.v3.Data.Channel();
                            _channel.Id = Guid.NewGuid().ToString();

                            _channel.Type = "web_hook";
                            _channel.Address = HttpContext.Current.Session["GoogleChannelAddress"].ToString() + @"/en/GoogleReceiver.aspx";
                            _channel.Token = HttpContext.Current.Session["userID"].ToString();

                            _chresp = new Google.Apis.Calendar.v3.Data.Channel();
                            _chresp = _service.Events.Watch(_channel, HttpContext.Current.Session["userEmail"].ToString()).Execute();

                        }
                        catch (Exception ex1)
                        {

                            _log.Trace(ex1.Message + " source " + ex1.StackTrace);
                        }



                    }


                    // Store and return the credentials.


                    _inXML = new StringBuilder();
                    _inXML.Append("<UserToken>");
                    _inXML.Append("<UserId>" + HttpContext.Current.Session["userID"].ToString() + "</UserId>");
                    _inXML.Append("<UserEmail>" + HttpContext.Current.Session["userEmail"].ToString() + "</UserEmail>");
                    _inXML.Append(_obj.OrgXMLElement());

                    _inXML.Append("<AccessToken>" + state.AccessToken.ToString() + "</AccessToken>");
                    HttpContext.Current.Session["GoogleAccessToken"] = _obj.GetEncrpytedText(HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString(), state.AccessToken.ToString().Trim());

                    if (string.IsNullOrEmpty(state.RefreshToken))
                        _inXML.Append("<RefreshToken>" + _obj.GetDecryptText(HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString(), HttpContext.Current.Session["GoogleRefreshToken"].ToString()) + "</RefreshToken>");
                    else
                    {
                        _inXML.Append("<RefreshToken>" + state.RefreshToken.ToString().Trim() + "</RefreshToken>");
                        HttpContext.Current.Session["GoogleRefreshToken"] = _obj.GetEncrpytedText(HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString(), state.RefreshToken.ToString().Trim());
                    }

                    if (_chresp != null)
                    {
                        _inXML.Append("<ChannelID>" + _chresp.Id + "</ChannelID>");
                        _inXML.Append("<ChannelEtag>" + _chresp.ETag + "</ChannelEtag>");

                        double _Expiration = 0;
                        double.TryParse(_chresp.Expiration.ToString(), out _Expiration);

                        DateTime _dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                        _dtDateTime = _dtDateTime.AddMilliseconds(_Expiration).ToUniversalTime();

                        if (_dtDateTime <= DateTime.MinValue)
                            _dtDateTime = DateTime.UtcNow;

                        _inXML.Append("<ChannelExpiration>" + _dtDateTime.ToString() + "</ChannelExpiration>");
                        _inXML.Append("<ChannelResourceId>" + _chresp.ResourceId + "</ChannelResourceId>");
                        _inXML.Append("<ChannelResourceUri>" + _chresp.ResourceUri + "</ChannelResourceUri>");
                        HttpContext.Current.Session["GoogleChannelID"] = "";
                        HttpContext.Current.Session["GoogleChannelAddress"] = "";
                        HttpContext.Current.Session["GoogleChanelExpiration"] = "";
                    }

                    _inXML.Append("</UserToken>");

                    _outXML = _obj.CallMyVRMServer("SetUserToken", _inXML.ToString(), Application["RTC_ConfigPath"].ToString());

                    XmlDocument errDoc = null;
                    if (_outXML.IndexOf("<error>") >= 0)
                    {
                        errDoc = new XmlDocument();
                        errDoc.LoadXml(_outXML);
                        if (errDoc.SelectSingleNode("error/message") != null)
                        {
                            HttpContext.Current.Response.Redirect("~/en/genlogin.aspx");

                        }
                    }

                    Session.Remove("SERVICE_OBJECT");
                    Session["SERVICE_OBJECT"] = _service;
                    HttpContext.Current.Session["AUTH_STATE"] = "";

                    //ZD 101344
                    if (Session["isExpressUser"] != null)


                        if (Session["isExpressUser"].ToString() == "1")
                        {
                            if (Session["LandingPageLink"] != null && Session["LandingPageLink"].ToString() != "" && _obj.checkURLSessionStatus(Session["LandingPageLink"].ToString())) // ZD 100172
                            {
                                Response.Redirect("~/en/" + Session["LandingPageLink"].ToString());
                            }
                            else
                                Response.Redirect("~/en/ExpressConference.aspx?t=n"); //FB 1830
                        }


                    if (Session["LandingPageLink"] != null && Session["LandingPageLink"].ToString() != "" && _obj.checkURLSessionStatus(Session["LandingPageLink"].ToString())) // ZD 100172
                        Response.Redirect("~/en/" + Session["LandingPageLink"].ToString());
                    else
                        Response.Redirect("~/en/SettingSelect2.aspx?c=GH");

                }
                else
                    HttpContext.Current.Response.Redirect("~/en/genlogin.aspx?G=A");


                return null;
            }
            catch (System.Threading.ThreadAbortException ex)
            {
                _log.Trace("" + ex.StackTrace);
                return null;
            }
            catch (Exception e)
            {
                _log.Trace("" + e.StackTrace);
                return null;
            }


        }
        #endregion


        #region setusertoken
        /// <summary>
        /// GetAuthorization
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        private void setusertoken(UserCredential cred, Google.Apis.Calendar.v3.Data.Channel _chresp)
        {
            //string currentGoogleEmail = "";
            try
            {
                if (_obj == null)
                    _obj = new myVRMNet.NETFunctions();

                if (cred.Token != null && (!string.IsNullOrEmpty(cred.Token.AccessToken) || !string.IsNullOrEmpty(cred.Token.RefreshToken)))
                {
                    //Checking the authenticated email account
                   // var accessToken = cred.Token.AccessToken;

                   // var urlBuilder = new System.Text.StringBuilder();

                   // urlBuilder.Append("https://");
                   // urlBuilder.Append("www.googleapis.com");
                   // urlBuilder.Append("/calendar/v3/users/me/calendarList");
                   // urlBuilder.Append("?minAccessRole=writer");

                   // var httpWebRequest = (HttpWebRequest)WebRequest.Create(urlBuilder.ToString());
                   // // Get the response.
                   // httpWebRequest.CookieContainer = new CookieContainer();
                   // httpWebRequest.Headers["Authorization"] =
                   //     string.Format("Bearer {0}", accessToken);

                   // var response1 = (HttpWebResponse)httpWebRequest.GetResponse();
                   // // Get the stream containing content returned by the server.
                   // var dataStream = response1.GetResponseStream();
                   // // Open the stream using a StreamReader for easy access.
                   // var reader = new StreamReader(dataStream);
                   // // Read the content. 
                   // var jsonString = reader.ReadToEnd();
                   // // Cleanup the streams and the response.
                   // reader.Close();
                   // dataStream.Close();
                   // response1.Close();

                   // XmlDocument doc = (XmlDocument)Newtonsoft.Json.JsonConvert.DeserializeXmlNode(jsonString, "Root");

                   // if (doc.SelectSingleNode("Root/items/id") != null)
                   //     currentGoogleEmail = doc.SelectSingleNode("Root/items/id").InnerText;

                   // if (currentGoogleEmail != HttpContext.Current.Session["userEmail"].ToString())
                   // {
                   //     //HttpContext.Current.Response.Redirect("~/" + HttpContext.Current.Session["language"].ToString() + "/GenLogin.aspx?G=f");
                   //     HttpContext.Current.Response.Redirect("~/en/GenLogin.aspx?G=f");
                   // }

                    
                   //// HttpContext.Current.Session["AUTH_STATE"] = _state = state;
                   // if (DateTime.Parse(HttpContext.Current.Session["GoogleChanelExpiration"].ToString()) < DateTime.UtcNow)
                   // {
                   //     try //ZD 102980
                   //     {
                   //         _channel = new Google.Apis.Calendar.v3.Data.Channel();
                   //         _channel.Id = Guid.NewGuid().ToString();

                   //         _channel.Type = "web_hook";
                   //         _channel.Address = HttpContext.Current.Session["GoogleChannelAddress"].ToString() + @"/en/GoogleReceiver.aspx";
                   //         _channel.Token = HttpContext.Current.Session["userID"].ToString();

                   //         _chresp = new Google.Apis.Calendar.v3.Data.Channel();
                   //         _chresp = _service.Events.Watch(_channel, HttpContext.Current.Session["userEmail"].ToString()).Execute();

                   //     }
                   //     catch (Exception ex1)
                   //     {

                   //         _log.Trace(ex1.Message + " source " + ex1.StackTrace);
                   //     }



                   // }
                    

                    // Store and return the credentials.


                    _inXML = new StringBuilder();
                    _inXML.Append("<UserToken>");
                    _inXML.Append("<UserId>" + HttpContext.Current.Session["userID"].ToString() + "</UserId>");
                    _inXML.Append("<UserEmail>" + HttpContext.Current.Session["userEmail"].ToString() + "</UserEmail>");
                    _inXML.Append(_obj.OrgXMLElement());

                    _inXML.Append("<AccessToken>" + cred.Token.AccessToken.ToString() + "</AccessToken>");
                    HttpContext.Current.Session["GoogleAccessToken"] = _obj.GetEncrpytedText(HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString(), cred.Token.AccessToken.ToString().Trim());

                    if (string.IsNullOrEmpty(cred.Token.RefreshToken))
                    {
                        //_inXML.Append("<RefreshToken>" + _obj.GetDecryptText(HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString(), HttpContext.Current.Session["GoogleRefreshToken"].ToString()) + "</RefreshToken>");
                        _inXML.Append("<RefreshToken></RefreshToken>");
                    }
                    else
                    {
                        _inXML.Append("<RefreshToken>" + cred.Token.RefreshToken.ToString().Trim() + "</RefreshToken>");
                        HttpContext.Current.Session["GoogleRefreshToken"] = _obj.GetEncrpytedText(HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString(), cred.Token.RefreshToken.ToString().Trim());
                    }

                    if (_chresp != null)
                    {
                        _inXML.Append("<ChannelID>" + _chresp.Id + "</ChannelID>");
                        _inXML.Append("<ChannelEtag>" + _chresp.ETag + "</ChannelEtag>");

                        double _Expiration = 0;
                        double.TryParse(_chresp.Expiration.ToString(), out _Expiration);

                        DateTime _dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                        _dtDateTime = _dtDateTime.AddMilliseconds(_Expiration).ToUniversalTime();

                        if (_dtDateTime <= DateTime.MinValue)
                            _dtDateTime = DateTime.UtcNow;

                        _inXML.Append("<ChannelExpiration>" + _dtDateTime.ToString() + "</ChannelExpiration>");
                        _inXML.Append("<ChannelResourceId>" + _chresp.ResourceId + "</ChannelResourceId>");
                        _inXML.Append("<ChannelResourceUri>" + _chresp.ResourceUri + "</ChannelResourceUri>");
                        HttpContext.Current.Session["GoogleChannelID"] = "";
                        HttpContext.Current.Session["GoogleChannelAddress"] = "";
                        HttpContext.Current.Session["GoogleChanelExpiration"] = "";
                    }

                    _inXML.Append("</UserToken>");

                    _outXML = _obj.CallMyVRMServer("SetUserToken", _inXML.ToString(), Application["RTC_ConfigPath"].ToString());

                    XmlDocument errDoc = null;
                    if (_outXML.IndexOf("<error>") >= 0)
                    {
                        errDoc = new XmlDocument();
                        errDoc.LoadXml(_outXML);
                        if (errDoc.SelectSingleNode("error/message") != null)
                        {
                            HttpContext.Current.Response.Redirect("~/en/genlogin.aspx");

                        }
                    }

                    Session.Remove("SERVICE_OBJECT");
                    Session["SERVICE_OBJECT"] = _service;
                    HttpContext.Current.Session["AUTH_STATE"] = "";

                    ////ZD 101344
                    //if (Session["isExpressUser"] != null)


                    //    if (Session["isExpressUser"].ToString() == "1")
                    //    {
                    //        if (Session["LandingPageLink"] != null && Session["LandingPageLink"].ToString() != "" && _obj.checkURLSessionStatus(Session["LandingPageLink"].ToString())) // ZD 100172
                    //        {
                    //            Response.Redirect("~/en/" + Session["LandingPageLink"].ToString());
                    //        }
                    //        else
                    //            Response.Redirect("~/en/ExpressConference.aspx?t=n"); //FB 1830
                    //    }


                    //if (Session["LandingPageLink"] != null && Session["LandingPageLink"].ToString() != "" && _obj.checkURLSessionStatus(Session["LandingPageLink"].ToString())) // ZD 100172
                    //    Response.Redirect("~/en/" + Session["LandingPageLink"].ToString());
                    //else
                    //    Response.Redirect("~/en/SettingSelect2.aspx?c=GH");

                }
                else
                    HttpContext.Current.Response.Redirect("~/en/genlogin.aspx?G=A");


                return;
            }
            catch (System.Threading.ThreadAbortException ex)
            {
                _log.Trace("" + ex.StackTrace);
                return;
            }
            catch (Exception e)
            {
                _log.Trace("" + e.StackTrace);
                return;
            }


        }
        #endregion

    }
}
