﻿//ZD 100147 start
/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.DirectoryServices;
using System.DirectoryServices.Protocols;
using System.Data;
using System.Xml;
using System.Text;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Xml.Schema;
using System.Collections.Generic;

namespace ns_LDAPGroup
{
    public partial class LDAPGroup : System.Web.UI.Page
    {

        #region protected data members

        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.DataGrid dgGroups;
        protected System.Web.UI.HtmlControls.HtmlButton btnSearch;
        protected System.Web.UI.HtmlControls.HtmlButton btnSubmitNew;
        protected System.Web.UI.HtmlControls.HtmlButton btnSubmit;
        protected System.Web.UI.HtmlControls.HtmlButton btnNew;
        protected System.Web.UI.WebControls.TextBox txtserveraddress;
        protected System.Web.UI.WebControls.TextBox txtDomain;
        protected System.Web.UI.WebControls.TextBox txtlogin;
        protected System.Web.UI.WebControls.TextBox txtPassword;
        protected System.Web.UI.WebControls.TextBox txtSearchGroups;
        protected System.Web.UI.WebControls.DropDownList lstOrganization;
        protected System.Web.UI.HtmlControls.HtmlInputHidden invokElem;
        protected System.Web.UI.WebControls.ListBox lstUserRoleList;
		//ZD 102182
        protected System.Web.UI.WebControls.Table tblPage;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnGroupRoles;
        protected System.Web.UI.WebControls.Label lblNoRecords;
        

        private myVRMNet.NETFunctions obj;
        private ns_Logger.Logger log;
        protected int userID = 11;
        private int iPort = 389;
        private int iSizeLimit = 500;
        private int AuthenticationType = 0;
        private int iTimeout = 10;
        private string loginKey = "";
        protected DataSet ds = null;
        XmlWriter _xWriter = null;
        XmlWriterSettings _xSettings = null;
        StringBuilder LDAPXML = null;
        private LdapConnection ldapConn;
        private int Tenetoption = 0; // AD groups change
        private int pageNo = 1;//ZD 102182
        Int32 totalPages;
        DataTable roleList;

        #endregion

        #region Constructor
        public LDAPGroup()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }

        #endregion

        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                base.InitializeCulture();
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();

                obj.AccessandURLConformityCheck("LDAPGroup.aspx", Request.Url.AbsoluteUri.ToLower());

                //ZD 102182
                if (Request.QueryString["pageNo"] != null)
                    pageNo = Int32.Parse(Request.QueryString["pageNo"].ToString());
                
                if (!IsPostBack)
                {
                    //ZD 102182
                    if (Request.QueryString["pageNo"] == null)
                    {
                        Session["AssignedGroupRole"] = null;
                        Session["SearchLDAPGroup"] = null;
                        Session["SelectedLDAPGroupOrg"] = null;
                        hdnGroupRoles.Value = "";
                    }

                    if (Session["SearchLDAPGroup"] != null)
                        txtSearchGroups.Text = Session["SearchLDAPGroup"].ToString();

                    if (Session["AssignedGroupRole"] != null)
                        hdnGroupRoles.Value = Session["AssignedGroupRole"].ToString();

                    obj.BindOrganizationNames(lstOrganization);

                    //ZD 102182
                    if (Session["SelectedLDAPGroupOrg"] != null && Session["SelectedLDAPGroupOrg"].ToString() != "")
                        lstOrganization.SelectedValue = Session["SelectedLDAPGroupOrg"].ToString();
                    else if (Session["organizationID"] != null && Session["organizationID"].ToString() != "")
                    {
                        lstOrganization.ClearSelection();
                        lstOrganization.SelectedValue = Session["organizationID"].ToString();
                    }

                    if (Session["UsrCrossAccess"] != null && Session["UsrCrossAccess"].ToString().Equals("1"))
                        lstOrganization.Enabled = true;
                    else
                        lstOrganization.Enabled = false;

                    if (BindData())
                        FetchLDAPGroup();
                }
                GetUserRolesData();//ZD 102045
                GetTemplateList();//ZD 102052

                //ZD 102182
                lstUserRoleList.DataSource = roleList;
                lstUserRoleList.DataTextField = "name";
                lstUserRoleList.DataValueField = "ID";
                lstUserRoleList.DataBind();
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.Message;
                errLabel.Visible = true;
                dgGroups.DataSource = null;
                dgGroups.DataBind();
            }
        }

        /*

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (BindData())
                    FetchLDAPGroup();
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.Message;
                errLabel.Visible = true;
                dgGroups.DataSource = null;
                dgGroups.DataBind();
            }
        }
        */
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
			//ZD 102052
            string inXML = "", outXML = "", RoleId = "";
            int Origin = 0;
            try
            {
                inXML = "<UpdateLDAPGroupRole>";
                inXML += obj.OrgXMLElement();
                inXML += "<SelectedOrgId>" + lstOrganization.SelectedItem.Value + "</SelectedOrgId>";
                inXML += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "<Groups>";
                TextBox txtrole = null;
                //ZD 102182
                if (hdnGroupRoles.Value != "")
                {
                    String[] strAssignedGpRoles = hdnGroupRoles.Value.Split('~');

                    //foreach (DataGridItem dgi in dgGroups.Items)

                    for (Int32 s = 0; s < strAssignedGpRoles.Length; s++)
                    {
                        String[] strAssRole = strAssignedGpRoles[s].Split('|');
                        //txtrole = (TextBox)dgi.FindControl("txtRoleid");
                        //if (txtrole != null && !string.IsNullOrWhiteSpace(txtrole.Text))
                        //{
                        inXML += "<Group>";
                        inXML += "<GroupId>" + strAssRole[0] + "</GroupId>";
                        inXML += "<GroupName>" + strAssRole[1] + "</GroupName>";
                        //ZD 102052 Starts
                        RoleId = "0";
                        String strRlID = strAssRole[2];
                        if (!string.IsNullOrEmpty(strRlID) && strRlID != "0")
                            RoleId = strRlID.Remove(0, 1);
                        if (!string.IsNullOrEmpty(strRlID) && strRlID != "0" && strRlID.Substring(0, 1) == "2")
                            Origin = 1;
                        else
                            Origin = 0;
                        if (strAssRole[4] == "U")
                        {
                            inXML += "<RoleId>" + RoleId + "</RoleId>";
                            inXML += "<Origin>" + Origin + "</Origin>";
                        }
                        else
                        {
                            inXML += "<RoleId>0</RoleId>";
                            inXML += "<Origin>0</Origin>";
                        }
                        //ZD 102052 End
                        inXML += "</Group>";
                        //}
                    }
                    inXML += "</Groups>";
                    inXML += "</UpdateLDAPGroupRole>";

                    inXML = inXML.Replace("&", "&amp;");//ZD 102045

                    outXML = obj.CallMyVRMServer("UpdateLDAPGroupRole", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        errLabel.Visible = true;
                        return;
                    }
                }
                //ZD 102182
                pageNo = 1;
                txtSearchGroups.Text = "";
                FetchLDAPGroupDetails("S");
                /*
                inXML = "<GetLDAPGroup><UserID>" + Session["userID"].ToString() + "</UserID>" + obj.OrgXMLElement() + "<SelectedOrgId>" + lstOrganization.SelectedItem.Value
                    + "</SelectedOrgId><pageNo>1</pageNo></GetLDAPGroup>"; //<Searchfor>" + txtSearchGroups.Text + "</Searchfor>
                outXML = obj.CallMyVRMServer("GetLDAPGroup", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                    return;
                }
                else
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    //ZD 102182
                    Session["AssignedGroupRole"] = null;
                    hdnGroupRoles.Value = "";
                    totalPages = Convert.ToInt32(xmldoc.SelectSingleNode("//GetLDAPGroup/totalPages").InnerText);
                    XmlNodeList nodeList = xmldoc.SelectNodes("//GetLDAPGroup/Groups/Group");
                    if (nodeList != null && nodeList.Count > 0)
                    {
                        LoadGrid(nodeList);
                        errLabel.Text = obj.ShowSuccessMessage();
                        errLabel.Visible = true;
                    }
                }
                */

            }
            catch (Exception ex)
            {
                errLabel.Text = ex.Message;
                errLabel.Visible = true;
            }
        }

        #region GetLdapConnection
        private LdapConnection GetLdapConnection(string sName, string domain, string uName, string pwd, int Port, string srcFilter)
        {
            LdapConnection lconnection = null;
            try
            {

                if (!(AuthenticateUser(sName, domain, uName, pwd, Port, srcFilter)))
                {
                    return lconnection;
                }

                LdapDirectoryIdentifier ldapDir = new LdapDirectoryIdentifier(sName, Port); //new design

                lconnection = new LdapConnection(ldapDir);
                //You may need to try different types of Authentication depending on your setup
                if (uName != "")
                {
                    lconnection.AuthType = AuthType.Ntlm;
                    System.Net.NetworkCredential myCredentials = new System.Net.NetworkCredential(uName, pwd, domain);
                    lconnection.Bind(myCredentials);
                }
                else
                    lconnection.AuthType = AuthType.Anonymous;

                lconnection.AutoBind = true;
                lconnection.Timeout = new TimeSpan(0, 0, 20, 0, 0);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lconnection;
        }
        #endregion

        #region AuthenticateUser
        private bool AuthenticateUser(string sAddress, string domain, string uName, string pwd, int Port, string srcFilter)
        {
            try
            {
                DirectoryEntry dirEntry = new DirectoryEntry();

                // specify the ldap server info
                string ldapPath = "LDAP://" + sAddress + ":" + Port;
                dirEntry.Path = ldapPath;

                //check to see if domain prefix exists
                if (domain != "")
                {
                    // prefix the domain before the username 
                    uName = domain + uName;
                }

                // specify ldap settings
                dirEntry.Username = uName.Trim();
                dirEntry.Password = pwd.Trim();
                // set auth type to "None"

                FetchAuthenticationType(ref dirEntry, ref AuthenticationType);// Ad group change

                // specify the search string
                DirectorySearcher dirSearcher = new DirectorySearcher(dirEntry);

                if (srcFilter != "")
                    dirSearcher.Filter = ("(&" + srcFilter + "(" + loginKey + "=" + uName.Trim() + "))");
                else
                    dirSearcher.Filter = "";

                dirSearcher.PropertiesToLoad.Add("givenname");
                dirSearcher.PropertiesToLoad.Add("sn");
                dirSearcher.PropertiesToLoad.Add("mail");
                dirSearcher.PropertiesToLoad.Add("cn");
                dirSearcher.PropertiesToLoad.Add("phone");
                dirSearcher.PropertiesToLoad.Add(loginKey);

                // run the search. check the result count to see there is only one record.
                // if multiple records are returned then username entered should not be considered as valid.
                SearchResult searchResult = dirSearcher.FindOne();

                return true;
            }
            catch (Exception e)
            {
                this.errLabel.Text = e.Message;
                errLabel.Visible = true;
                log.Trace(e.Message);
                return false;
            }
        }
        #endregion

        #region GetRootDN
        private string GetRootDN(LdapConnection conn)
        {
            string rootDN = "";
            bool isRootDN = false;
            try
            {
                log.Trace("Into SearchRequest in Root Node");
                SearchRequest findme = new SearchRequest();
                findme.DistinguishedName = ""; //Find all People in this ou
                findme.Scope = System.DirectoryServices.Protocols.SearchScope.Base; //We want BASE

                SearchResponse results = (SearchResponse)ldapConn.SendRequest(findme); //Run the query and get results

                SearchResultEntryCollection entries = results.Entries;
                log.Trace("Total Count of Entries :" + entries.Count.ToString());
                for (int i = 0; i < entries.Count; i++)//Iterate through the results
                {
                    SearchResultEntry entry = entries[i];
                    IDictionaryEnumerator attribEnum = entry.Attributes.GetEnumerator();

                    while (attribEnum.MoveNext())//Iterate through the result attributes
                    {
                        DirectoryAttribute subAttrib = (DirectoryAttribute)attribEnum.Value;
                        for (int ic = 0; ic < subAttrib.Count; ic++)
                        {
                            //Attribute Name below
                            if (attribEnum.Key.ToString().Equals("defaultnamingcontext") || attribEnum.Key.ToString().Equals("rootdomainnamingcontext") || attribEnum.Key.ToString().Equals("namingcontexts"))//PSU Issue
                            {
                                rootDN = subAttrib[ic].ToString();
                                isRootDN = true;
                                if (!(Application["Client"].ToString().ToUpper() == "PSU"))
                                {
                                    break;
                                }
                            }

                            string output = attribEnum.Key.ToString() + " = " + subAttrib[ic].ToString();
                            log.Trace("GetRootDN" + output);
                        }
                        if (isRootDN)
                            break;
                    }
                    if (isRootDN)
                        break;
                }
                if (rootDN == "")
                    rootDN = "LDAP";
            }
            catch (Exception ex)
            {
                log.Trace("Into exception in Root Node :" + ex.Message.ToUpper());
                throw ex;
            }
            return rootDN;
        }
        #endregion

        #region BindData

        protected bool BindData()
        {
            try
            {
                string inXML = "<login>";
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <OrgID>" + lstOrganization.SelectedItem.Value.ToString() + "</OrgID>";
                inXML += "  </login>";
                string outxml = obj.CallMyVRMServer("GetOrgLDAP", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outxml.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.GetTranslatedText("Active Directory or LDAP Directory not configured in Organization Settings");
                    errLabel.Visible = true;
                    txtserveraddress.Text = "";
                    txtlogin.Text = "";
                    txtPassword.Text = "";
                    txtDomain.Text = "";
                    loginKey = "";
                    AuthenticationType = 0;
                    dgGroups.DataSource = null;
                    dgGroups.DataBind();
                    return false;
                }
                else
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outxml);
                    txtserveraddress.Text = xmldoc.SelectSingleNode("//GetOrgLDAP/ServerDetails/ServerAddress").InnerText.Trim();
                    txtlogin.Text = xmldoc.SelectSingleNode("//GetOrgLDAP/ServerDetails/Username").InnerText.Trim();
                    txtPassword.Text = xmldoc.SelectSingleNode("//GetOrgLDAP/ServerDetails/Password").InnerText.Trim();
                    txtDomain.Text = xmldoc.SelectSingleNode("//GetOrgLDAP/ServerDetails/Domain").InnerText.Trim();
                    //srcFilter = xmldoc.SelectSingleNode("//GetOrgLDAP/ServerDetails/SearchFilter").InnerText.Trim();
                    //lblSrchFilter.Text = xmldoc.SelectSingleNode("//GetOrgLDAP/ServerDetails/SearchFilter").InnerText.Trim();
                    loginKey = xmldoc.SelectSingleNode("//GetOrgLDAP/ServerDetails/LoginKey").InnerText.Trim();
                    int.TryParse(xmldoc.SelectSingleNode("//GetOrgLDAP/ServerDetails/AuthenticationType").InnerText.Trim(), out AuthenticationType);
                    if (xmldoc.SelectSingleNode("//GetOrgLDAP/ServerDetails/ServerPort") != null)
                        int.TryParse(xmldoc.SelectSingleNode("//GetOrgLDAP/ServerDetails/ServerPort").InnerText.Trim(), out iPort);
                    if (xmldoc.SelectSingleNode("//GetOrgLDAP/ServerDetails/ServerTimeOut") != null)
                        int.TryParse(xmldoc.SelectSingleNode("//GetOrgLDAP/ServerDetails/ServerTimeOut").InnerText.Trim(), out iTimeout);
                    if (xmldoc.SelectSingleNode("//GetOrgLDAP/ServerDetails/TenantOption") != null)
                        int.TryParse(xmldoc.SelectSingleNode("//GetOrgLDAP/ServerDetails/TenantOption").InnerText.Trim(), out Tenetoption); // AD groups change
                    
                    errLabel.Visible = false;
                }
            }
            catch (Exception ex)
            {
                log.Trace("BindData" + ex.Message);
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();
                dgGroups.DataSource = null;
                dgGroups.DataBind();
                return false;
            }

            return true;
        }

        #endregion

        #region Change Organization LDAP
        protected void ChangeOrgLDAP(object sender, EventArgs e)
        {
            try
            {
                txtSearchGroups.Text = "";
                Session["SelectedLDAPGroupOrg"] = lstOrganization.SelectedItem.Value;
                hdnGroupRoles.Value = "";
                Session["AssignedGroupRole"] = null;
                Session["SearchLDAPGroup"] = null;

                if (BindData())
                    FetchLDAPGroup();
                else
                {
                    dgGroups.DataSource = null;
                    dgGroups.DataBind();
                }

            }
            catch (Exception ex)
            {
                errLabel.Text = ex.Message;
                errLabel.Visible = true;
            }
        }
        #endregion

        //ZD 102182
        protected void UpdateSessionData(object sender, EventArgs e)
        {
            Session["AssignedGroupRole"] = hdnGroupRoles.Value;
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "resumeControlFlow", "<script>fnSessionUpdateResponse()</script>", false);
        }

        #region FetchLDAPGroup

        protected void FetchLDAPGroup()
        {
            // AD groups change
            LDAPXML = new StringBuilder();
            string inxml = "", outXML = "";
            DirectoryEntry objDE = null; 
            DirectorySearcher objDSR = null;
            SearchResultCollection objSrchResults = null;
            SearchResult objSearchResult = null;
            string group = "";
            DataTable dt = new DataTable();//ZD 102182
            try
            {
                errLabel.Visible = false;
                objDE = new DirectoryEntry();
                objDE.Username = txtDomain.Text + txtlogin.Text;
                objDE.Password = txtPassword.Text;
                if (Tenetoption == 2)
                    objDE.Path = "GC://" + txtserveraddress.Text + ":" + iPort.ToString();
                else
                    objDE.Path = "LDAP://" + txtserveraddress.Text + ":" + iPort.ToString();
                FetchAuthenticationType(ref objDE, ref AuthenticationType);
                objDSR = new DirectorySearcher(objDE);
                objDSR.Filter = "(&(objectClass=group))";
                objDSR.SearchScope = System.DirectoryServices.SearchScope.Subtree;                
                objDSR.PageSize = 1000; //ZD 102182
                objSrchResults = objDSR.FindAll();

                if (objSrchResults != null)
                {
                    log.Trace("Entries Count :" + objSrchResults.Count.ToString());

                    _xSettings = new XmlWriterSettings();
                    _xSettings.OmitXmlDeclaration = true;
                    using (_xWriter = XmlWriter.Create(LDAPXML, _xSettings))
                    {
                        _xWriter.WriteStartElement("SetLDAPGroup");
                        _xWriter.WriteElementString("UserID", Session["userID"].ToString());
                        _xWriter.WriteString(obj.OrgXMLElement());
                        _xWriter.WriteElementString("SelectedOrgId", lstOrganization.SelectedItem.Value);
                        _xWriter.WriteStartElement("Groups");

                        for (int i = 0; i < objSrchResults.Count; i++)//Iterate through the results
                        {
                            objSearchResult = null;
                            group = "";
                            objSearchResult = objSrchResults[i];
                            group = objSearchResult.Properties["cn"][0].ToString();
                            _xWriter.WriteElementString("Group", group);
                        }
                        _xWriter.WriteFullEndElement();
                        _xWriter.WriteFullEndElement();
                        _xWriter.Flush();
                    }

                    LDAPXML = LDAPXML.Replace("&lt;", "<")
                                   .Replace("&gt;", ">").Replace("&", "&amp;");

                    outXML = obj.CallMyVRMServer("SetLDAPGroup", LDAPXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        errLabel.Visible = true;
                        return;
                    }
                    else
                    {
                        FetchLDAPGroupDetails(""); //ZD 102182
                    }
                }

            }
            catch (Exception ex)
            {
                log.Trace("BindData" + ex.Message);
                errLabel.Text = ex.Message;
                errLabel.Visible = true;
                dgGroups.DataSource = null;
                dgGroups.DataBind();
            }
        }

        #endregion

        //ZD 102182 - Start

        protected void FetchLDAPGroupDetails(String strSuccess)
        {
            string inxml = "", outXML = "";
            DataTable dt = new DataTable();

            try
            {
                String strSearch = txtSearchGroups.Text;
                strSearch = strSearch.Replace("&", "&amp;");

                inxml = "<GetLDAPGroup><UserID>" + Session["userID"].ToString() + "</UserID>" + obj.OrgXMLElement() + "<Searchfor>" + strSearch + "</Searchfor><SelectedOrgId>" + lstOrganization.SelectedItem.Value
                        + "</SelectedOrgId><pageNo>" + pageNo + "</pageNo></GetLDAPGroup>";
                outXML = obj.CallMyVRMServer("GetLDAPGroup", inxml, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                    return;
                }
                else
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);

                    totalPages = Convert.ToInt32(xmldoc.SelectSingleNode("//GetLDAPGroup/totalPages").InnerText);//ZD 102182

                    XmlNodeList nodeList = xmldoc.SelectNodes("//GetLDAPGroup/Groups/Group");

                    lblNoRecords.Visible = false;
                    if (nodeList != null && nodeList.Count > 0)
                        LoadGrid(nodeList);
                    else
                    {
                        dgGroups.DataSource = null;
                        dgGroups.DataBind();
                        lblNoRecords.Visible = true;
                    }

                    //ZD 102182
                    if (strSuccess == "S")
                    {
                        errLabel.Text = obj.ShowSuccessMessage();
                        errLabel.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("FetchLDAPGroupDetails" + ex.Message);
                errLabel.Text = ex.Message;
                errLabel.Visible = true;
                dgGroups.DataSource = null;
                dgGroups.DataBind();
            }
        }

        protected void SearchLDAPGroup(object sender, EventArgs e)
        {
            try
            {
                pageNo = 1;
                if (txtSearchGroups.Text.Trim() != "")
                {
                    Session["SearchLDAPGroup"] = txtSearchGroups.Text.Trim();
                    if (BindData())
                        FetchLDAPGroup();
                }
                else
                {
                    Session["SearchLDAPGroup"] = null;
                    FetchLDAPGroupDetails("");
                }
            }
            catch (Exception ex)
            {
                this.errLabel.Text = ex.Message;
                errLabel.Visible = true;
                log.Trace(ex.Message);
                dgGroups.DataSource = null;
                dgGroups.DataBind();
            }
        }
        //ZD 102182 - End

        #region LoadGrid
        /// <summary>
        /// LoadGrid
        /// </summary>
        /// <param name="NodeList"></param>
        /// <returns></returns>
        private bool LoadGrid(XmlNodeList NodeList)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();//ZD 102182

                foreach (XmlNode node in NodeList)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
				//ZD 102182
                dt = ds.Tables[0];

                if (!dt.Columns.Contains("RoleName"))
                    dt.Columns.Add("RoleName");

                if (!dt.Columns.Contains("GroupId"))
                    dt.Columns.Add("GroupId");
                //ZD 102052 Starts
                string roleStr = obj.GetTranslatedText("R");
                string tempStr = obj.GetTranslatedText("T");

                String[] tmpArray = null;
                String[] strGroupRoles = null;

                if (hdnGroupRoles.Value != "")
                {
                    strGroupRoles = hdnGroupRoles.Value.Split('~');

                    tmpArray = new string[strGroupRoles.Length];

                    for (int r = 0; r < strGroupRoles.Length; r++)
                    {
                        tmpArray[r] = strGroupRoles[r].Split('|')[1];
                    }
                }

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["Origin"].ToString() == "1")
                    {
                        if (dt.Rows[i]["RoleName"].ToString() != "")
                            dt.Rows[i]["RoleName"] = dt.Rows[i]["RoleName"] + "(" + tempStr + ")";
                        dt.Rows[i]["RoleId"] = "2" + dt.Rows[i]["RoleId"].ToString();
                    }
                    else
                    {
                        if (dt.Rows[i]["RoleName"].ToString() != "")
                            dt.Rows[i]["RoleName"] = dt.Rows[i]["RoleName"] + "(" + roleStr + ")";
                        dt.Rows[i]["RoleId"] = "1" + dt.Rows[i]["RoleId"].ToString();
                    }

                    if (tmpArray != null)
                    {
                        int index = Array.FindIndex(tmpArray, delegate(string s) { return s.Equals(dt.Rows[i]["GroupName"].ToString()); });

                        if (index > -1)
                        {
                            if (dt.Rows[i]["GroupName"].ToString() == strGroupRoles[index].Split('|')[1] )
                            {
                                if (strGroupRoles[index].Split('|')[4] == "U")
                                {
                                    dt.Rows[i]["RoleId"] = strGroupRoles[index].Split('|')[2];
                                    dt.Rows[i]["RoleName"] = strGroupRoles[index].Split('|')[3];
                                }
                                else
                                {
                                    dt.Rows[i]["RoleId"] = "";
                                    dt.Rows[i]["RoleName"] = "";
                                }
                            }
                        }
                    }
                }

                //ZD 102052 End
                if (totalPages > 1)
                {
                    obj.DisplayPaging(totalPages, pageNo, tblPage, "LDAPGroup.aspx?1=1");
                }

                dgGroups.DataSource = null;
                dgGroups.DataBind();

                dgGroups.DataSource = dt;
                dgGroups.DataBind();
            }
            catch (Exception ex)
            {
                this.errLabel.Text = ex.Message;
                errLabel.Visible = true;
                log.Trace(ex.Message);
                dgGroups.DataSource = null;
                dgGroups.DataBind();
                return false;
            }
            return true;

        }
        #endregion


        #region GetUserRolesData

        private bool GetUserRolesData()
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                XmlNodeList nodeList = null;

                string inXML = "<login>" + obj.OrgXMLElement() +
                        "  <userID>" + Session["userID"].ToString() + "</userID>" +
                        "  <foodModuleEnable>" + Session["foodModule"].ToString() + "  </foodModuleEnable>" +
                        "  <roomModuleEnable>" + Session["roomModule"].ToString() + "  </roomModuleEnable>" +
                        "  <hkModuleEnable>" + Session["hkModule"].ToString() + "  </hkModuleEnable>" +
                        "</login>";

                string outXML = obj.CallMyVRMServer("GetUserRoles", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") > 0)
                {
                    errLabel.Text = "Issue in fetching User Roles.";
                    errLabel.Visible = true;
                    return false;
                }

                xmlDoc.LoadXml(outXML);
                nodeList = xmlDoc.SelectNodes("//roles/role");
                if (nodeList != null && nodeList.Count > 0)
                {
                    XmlTextReader xtr;
                    DataSet ds = new DataSet();

                    foreach (XmlNode node in nodeList)
                    {
                        xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                        ds.ReadXml(xtr, XmlReadMode.InferSchema);
                    }
                    if (!ds.Tables[0].Columns.Contains("ID"))
                        ds.Tables[0].Columns.Add("ID");
                    if (!ds.Tables[0].Columns.Contains("name"))
                        ds.Tables[0].Columns.Add("name");
                    if (!ds.Tables[0].Columns.Contains("createType"))
                        ds.Tables[0].Columns.Add("createType");
                    if (!ds.Tables[0].Columns.Contains("crossaccess"))
                        ds.Tables[0].Columns.Add("crossaccess");
                    if (!ds.Tables[0].Columns.Contains("level"))
                        ds.Tables[0].Columns.Add("level");

                    string roleStr = obj.GetTranslatedText("R");//ZD 102052
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        if (Session["UsrCrossAccess"].ToString() != "1" || lstOrganization.SelectedItem.Value != "11" || (Session["organizationID"] != null && Session["organizationID"].ToString() != "11")) //ZD 102045-Removed site role for switch org users in LDAP Group page.
                        {
                            if (ds.Tables[0].Rows[i]["crossaccess"].ToString() == "1" && ds.Tables[0].Rows[i]["level"].ToString() == "2")
                            {
                                ds.Tables[0].Rows.Remove(ds.Tables[0].Rows[i]);
                                i++;
                                continue;
                            }
                        }

                        if (ds.Tables[0].Rows[i]["createType"] != null)
                        {
                            if (ds.Tables[0].Rows[i]["createType"].ToString() == "1")
                                ds.Tables[0].Rows[i]["name"] = obj.GetTranslatedText(ds.Tables[0].Rows[i]["name"].ToString()) + "(" + roleStr + ")";//ZD 102052
                            else
                                ds.Tables[0].Rows[i]["name"] = ds.Tables[0].Rows[i]["name"] + obj.GetTranslatedText("(Custom)") + "(" + roleStr + ")";//ZD 102052
                        }
						//ZD 102052 Starts
                        if (ds.Tables[0].Rows[i]["ID"] != null)
                            ds.Tables[0].Rows[i]["ID"] = "1" + ds.Tables[0].Rows[i]["ID"].ToString();
						//ZD 102052 End
                    }
					//ZD 102052 Starts
                    //lstUserRoleList.DataSource = ds;
                    //lstUserRoleList.DataTextField = "name";
                    //lstUserRoleList.DataValueField = "ID";
                    //lstUserRoleList.DataBind();
                    ViewState["roleList"] = ds.Tables[0];//ZD 102052
					//ZD 102052 End
                }

            }
            catch (Exception ex)
            {
                log.Trace("GetUserRolesData" + ex.Message);
            }
            return true;
        }
        #endregion

        //ZD 102052 START
        #region GetTemplateList
        private bool GetTemplateList()
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                XmlNodeList nodeList = null;

                string inXML = "<login>";
                inXML += obj.OrgXMLElement();
                inXML += "<userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "</login>";

                string outXML = obj.CallMyVRMServer("GetUserTemplateList", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                xmlDoc.LoadXml(outXML);
                nodeList = xmlDoc.SelectNodes("/UserTemplates/Template");
                //ZD 102182
                roleList = (DataTable)ViewState["roleList"];

                if (nodeList != null && nodeList.Count > 0)
                {
                    XmlTextReader xtr;
                    DataSet dstemplate = new DataSet();

                    foreach (XmlNode node in nodeList)
                    {
                        xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                        dstemplate.ReadXml(xtr, XmlReadMode.InferSchema);
                    }

                    if (dstemplate != null && dstemplate.Tables.Count > 0)
                    {
                        if (!dstemplate.Tables[0].Columns.Contains("ID"))
                            dstemplate.Tables[0].Columns.Add("ID");
                        if (!dstemplate.Tables[0].Columns.Contains("Name"))
                            dstemplate.Tables[0].Columns.Add("Name");

                        DataTable templateList = dstemplate.Tables[0];
                        string tempStr = obj.GetTranslatedText("T");

                        if (templateList.Rows.Count > 0)
                        {
                            for (int i = 0; i < templateList.Rows.Count; i++)
                            {
                                if (templateList.Rows[i]["Name"] != null)
                                {
                                    templateList.Rows[i]["Name"] = templateList.Rows[i]["Name"].ToString() + "(" + tempStr + ")";
                                }

                                if (templateList.Rows[i]["ID"] != null)
                                {
                                    templateList.Rows[i]["ID"] = "2" + templateList.Rows[i]["ID"].ToString();
                                }
                            }
                            roleList.Merge(templateList);
                        }
                    }

                    
                }
            }
            catch (Exception ex)
            {
                log.Trace("GetTemplateList" + ex.Message);
            }
            return true;
        }

        #endregion
        //ZD 102052 END

        #region Switch Authentication Type

        public void FetchAuthenticationType(ref DirectoryEntry dirEntry, ref int AuthenticationType)
        {
            // AD groups change
            switch (AuthenticationType)
            {
                case 0:
                    dirEntry.AuthenticationType = AuthenticationTypes.None;
                    break;
                case 1:
                    dirEntry.AuthenticationType = AuthenticationTypes.Signing;
                    break;
                case 2:
                    dirEntry.AuthenticationType = AuthenticationTypes.Sealing;
                    break;
                case 3:
                    dirEntry.AuthenticationType = AuthenticationTypes.Secure;
                    break;
                case 4:
                    dirEntry.AuthenticationType = AuthenticationTypes.Anonymous;
                    break;
                case 5:
                    dirEntry.AuthenticationType = AuthenticationTypes.Delegation;
                    break;
                case 6:
                    dirEntry.AuthenticationType = AuthenticationTypes.Encryption;
                    break;
                case 7:
                    dirEntry.AuthenticationType = AuthenticationTypes.FastBind;
                    break;
                case 8:
                    dirEntry.AuthenticationType = AuthenticationTypes.ReadonlyServer;
                    break;
                case 9:
                    dirEntry.AuthenticationType = AuthenticationTypes.SecureSocketsLayer;
                    break;
                case 10:
                    dirEntry.AuthenticationType = AuthenticationTypes.ServerBind;
                    break;
                default:
                    dirEntry.AuthenticationType = AuthenticationTypes.None;
                    break;
            }
        }

        #endregion


    }
}