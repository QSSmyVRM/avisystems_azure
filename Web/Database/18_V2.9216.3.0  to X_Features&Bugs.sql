/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9216.3.0  Starts(1st April 2016)         */
/*                                                                                              */
/* ******************************************************************************************** */


/* **********************ALLDEV-498 - 1st April 2016 Starts************ */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	ConfAdministrator varchar(MAX) NULL
GO
ALTER TABLE dbo.Org_Settings_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


update Org_Settings_D set ConfAdministrator = ''


/* **********************ALLDEV-498 -1st April 2016 2015 Ends************ */


/* **********************ALLDEV-839 - 4th April 2016 Starts************ */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	AllowRequestortoEdit smallint NULL
GO
ALTER TABLE dbo.Org_Settings_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


update Org_Settings_D set AllowRequestortoEdit = 0


/* **********************ALLDEV-839 - 4th April 2016 Ends************ */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9216.3.0  Ends  (12th April 2016)         */
/*                              Features & Bugs for V2.9216.3.1  Starts(12th April 2016)         */
/*                                                                                              */
/* ******************************************************************************************** */

/* **********************ALLDEV-807 - 13th April 2016 Starts************ */
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Loc_Assistant_D
	(
	ID int NOT NULL IDENTITY (1, 1),
	RoomId int NOT NULL,
	AssistantId int NOT NULL,
	EmailId nvarchar(250) NULL,
	AssitantName nvarchar(250) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Loc_Assistant_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

Insert into Loc_Assistant_D (RoomId,AssistantId,EmailId,AssitantName) 
select RoomID,Assistant,GuestContactEmail,GuestContactName from Loc_Room_D

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Loc_Room_D ADD
	isBusy int NULL
GO
ALTER TABLE dbo.Loc_Room_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


Update Loc_Room_D set isBusy =0 


IF EXISTS (
SELECT * 
FROM sys.indexes 
WHERE name='IX_Usr_List_D_Email' AND object_id = OBJECT_ID('Usr_List_D'))
Begin
DROP INDEX [IX_Usr_List_D_Email] ON Usr_List_D;
ENd

ALTER TABLE Usr_Inactive_D ALTER COLUMN Email  nvarchar(255)  NULL
ALTER TABLE Usr_List_D ALTER COLUMN Email  nvarchar(255)  NULL
ALTER TABLE Usr_GuestList_D ALTER COLUMN Email  nvarchar(255)  NULL




/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Archive_Conf_Conference_D ADD
	isHDBusy smallint NULL
GO
ALTER TABLE dbo.Archive_Conf_Conference_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

Update Archive_Conf_Conference_D set isHDBusy = 0

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	isHDBusy int NULL
GO
ALTER TABLE dbo.Conf_Conference_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

Update Conf_Conference_D set isHDBusy = 0

--Update  Icons_Ref_S set IconTarget ='ConferenceList.aspx?t=10' where IconID = 4 --Public Conference


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Audit_Loc_Room_D ADD
	isBusy int NULL
GO
ALTER TABLE dbo.Audit_Loc_Room_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


Update Audit_Loc_Room_D set isBusy = 0



CREATE TABLE [dbo].[Audit_Loc_Assistant_D](
	ID int NOT NULL IDENTITY (1, 1),
	RoomId int NOT NULL,
	AssistantId int NOT NULL,
	EmailId nvarchar(250) NULL,
	AssitantName nvarchar(250) NULL,
	[LastModifiedDate] [datetime] NULL
	)  ON [PRIMARY]

Insert into [Audit_Loc_Assistant_D] ([RoomId], AssistantId, EmailId,AssitantName,[LastModifiedDate])
( select [RoomId], AssistantId,EmailId, AssitantName,GETUTCDATE() from Loc_Assistant_D)



insert into icons_ref_s values('53','../en/img/PublicConferenceIcon.png','My Hotdesk(s)','ConferenceList.aspx?t=10')

/* **********************ALLDEV-807 - 13th April 2016 Ends************ */


/* **********************ALLDEV-854 - 19th April 2016  Starts************ */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.MCU_List_D ADD
	EnableAlias smallint NULL,
	Alias2 nvarchar(MAX) NULL,
	Alias3 nvarchar(MAX) NULL
GO
COMMIT


update MCU_List_D set EnableAlias = 0, Alias2='', Alias3='' 

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Audit_Mcu_List_D ADD
	EnableAlias smallint NULL,
	Alias2 nvarchar(MAX) NULL,
	Alias3 nvarchar(MAX) NULL
GO
COMMIT


update Audit_Mcu_List_D set EnableAlias = 0, Alias2='', Alias3=''
 

/* **********************ALLDEV-854 - 19th April 2016 Ends************ */