﻿//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
#region using
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections.Specialized;
using Microsoft.Exchange.WebServices.Data;
using System.Net;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Security.Cryptography.X509Certificates;
using System.Linq;
using System.Xml;
using System.IO;
using System.Security.Cryptography;
using System.Globalization;
using Microsoft.Office.Interop.Outlook;
using Microsoft.Office.Tools.Outlook;
using myVRMEWS.ExchangeReference;
using myVRMEWS.PushService;
#endregion

namespace myVRMEWS
{
    public class BookAndFindRelatedAppoitnmentTest
    {
        public const string ExchangeWebServiceUrl = "https://contoso.com/ews/Exchange.asmx";

      
        public void TestThatAppointmentsAreRelated()
        {
            ExchangeService service = GetExchangeService();

            //Impersonate the user who is creating the appiontment request
            service.ImpersonatedUserId = new ImpersonatedUserId(ConnectingIdType.PrincipalName, "Test1");
            Appointment apptRequest = CreateAppointmentRequest(service, new Attendee("Test2@contoso.com"));

            //After the appointment is created, we must rebind the data for the appointment in order to set the Unique Id
            apptRequest = Appointment.Bind(service, apptRequest.Id);

            //Impersonate the Attendee and locate the appointment on their calendar
            service.ImpersonatedUserId = new ImpersonatedUserId(ConnectingIdType.PrincipalName, "Test2");
            //Sleep for a second to let the meeting request propogate YMMV so you may need to increase the sleep for this test
            System.Threading.Thread.Sleep(1000);
            Appointment relatedAppt = FindRelatedAppiontment(service, apptRequest);

            //Assert.AreNotEqual(apptRequest.Id, relatedAppt.Id);
            //Assert.AreEqual(apptRequest.ICalUid, relatedAppt.ICalUid);
        }

        private static Appointment CreateAppointmentRequest(ExchangeService service, params Attendee[] attendees)
        {
            // Create the appointment.
            Appointment appointment = new Appointment(service)
            {
                // Set properties on the appointment.
                Subject = "Test Appointment",
                Body = "Testing Exchange Services and Appointment relationships.",
                Start = DateTime.Now,
                End = DateTime.Now.AddHours(1),
                Location = "Your moms house",
            };

            //Add the attendess
            Array.ForEach(attendees, a => appointment.RequiredAttendees.Add(a));

            // Save the appointment and send out invites
            appointment.Save(SendInvitationsMode.SendToAllAndSaveCopy);

            return appointment;
        }

        /// <summary>
        /// Finds the related appiontment.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="apptRequest">The appt request.</param>
        /// <returns></returns>
        private static Appointment FindRelatedAppiontment(ExchangeService service, Appointment apptRequest)
        {
            var filter = new SearchFilter.IsEqualTo
            {
                PropertyDefinition = new ExtendedPropertyDefinition
                  (DefaultExtendedPropertySet.Meeting, 0x03, MapiPropertyType.Binary),
                Value = GetObjectIdStringFromUid(apptRequest.ICalUid) //Hex value converted to byte and base64 encoded
            };

            var view = new ItemView(1) { PropertySet = new PropertySet(BasePropertySet.FirstClassProperties) };

            return service.FindItems(WellKnownFolderName.Calendar, filter, view).Items[0] as Appointment;
        }

        /// <summary>
        /// Gets the exchange service.
        /// </summary>
        /// <returns></returns>
        private static ExchangeService GetExchangeService()
        {
            //You can use AutoDiscovery also but in my scenario, I have it turned off      
            return new ExchangeService(ExchangeVersion.Exchange2007_SP1)
            {
                Credentials = new System.Net.NetworkCredential("dan.test", "Password1"),
                Url = new Uri(ExchangeWebServiceUrl)
            };
        }

        /// <summary>
        /// Gets the object id string from uid.
        /// <remarks>The UID is formatted as a hex-string and the GlobalObjectId is displayed as a Base64 string.</remarks>
        /// </summary>
        /// <param name="id">The uid.</param>
        /// <returns></returns>
        private static string GetObjectIdStringFromUid(string id)
        {
            var buffer = new byte[id.Length / 2];
            for (int i = 0; i < id.Length / 2; i++)
            {
                var hexValue = byte.Parse(id.Substring(i * 2, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
                buffer[i] = hexValue;
            }
            return Convert.ToBase64String(buffer);
        }
    }
}
