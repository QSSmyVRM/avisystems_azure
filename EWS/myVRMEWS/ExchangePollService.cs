﻿//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Specialized;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.Xml;
using myVRMEWS.ExchangeReference;

namespace myVRMEWS
{
    public partial class myVRMEWSService : ServiceBase
    {
        System.Timers.Timer pollConferences = new System.Timers.Timer();
        System.Globalization.CultureInfo globalCultureInfo = new System.Globalization.CultureInfo("en-US", true);//ZD 104846
        System.Timers.Timer timerAutoPurgeLogs = new System.Timers.Timer(); //ZD 104846
        static int purgeDuration = 0; //ZD 104846
        String MyVRMServer_ConfigPath = "", myVRMURL = "", myVRMUsername = "", myVRMPassword = "", myVRMVersion = "", myVRMClient = "", SSOMode = "", filePath; //ZD 104846

        public myVRMEWSService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            double pollConfs = 30000;
            //ZD 104846 Start
            double purgeLogsInterval = 24 * 60 * 60 * 1000; 
            System.Configuration.ConnectionStringSettings conSettings = null; 
            string connectionString = ""; 
            NameValueCollection appSettings = null; 
            string[] Values = null; 
            //ZD 104846 End
            try
            {
                
                pollConferences.Elapsed += new System.Timers.ElapsedEventHandler(pollConferences_Elapsed);
                pollConferences.Interval = pollConfs;
                pollConferences.AutoReset = false;
                pollConferences.Start();

                //ZD 104846 Start 
                timerAutoPurgeLogs.Elapsed += new System.Timers.ElapsedEventHandler(timerAutoPurgelogs_Elapsed);
                timerAutoPurgeLogs.Interval = purgeLogsInterval;
                timerAutoPurgeLogs.Enabled = true;
                timerAutoPurgeLogs.Start();

                conSettings = ConfigurationManager.ConnectionStrings["MyDBConnectionString"];
                connectionString = conSettings.ConnectionString;
                appSettings = ConfigurationManager.AppSettings;
                Values = new string[appSettings.Count];
                for (int count = 0; count < appSettings.Count; count++)
                {
                    Values[count] = appSettings[count];
                }
                filePath = Values[6];
                myVRMURL = Values[9];
                myVRMUsername = Values[10];
                myVRMPassword = Values[11];
                myVRMVersion = Values[12];
                myVRMClient = "07";
                if (!string.IsNullOrEmpty(appSettings["SSOMode"]))
                    SSOMode = appSettings["SSOMode"];
                //ZD 104846 End
            }
            catch (Exception)
            {

            }

        }

        protected override void OnStop()
        {
            pollConferences.Enabled = false;
            pollConferences.AutoReset = false;
            pollConferences.Stop();
        }

        private void pollConferences_Elapsed(object sender, EventArgs e)
        {
            double pollConfs = 30000;
            try
            {
                pollConferences.Stop();

                ExcahngePollNms.ExcahngePoll exchangePollService = new ExcahngePollNms.ExcahngePoll();
                exchangePollService.start();

            }
            catch (Exception)
            {
            }

            pollConferences.Interval = pollConfs;
            pollConferences.AutoReset = false;
            pollConferences.Start();

        }

        //ZD 104846 start
        #region timerAutoPurgelogs_Elapsed
        /// <summary>
        /// timerAutoPurgelogs_Elapsed
        /// </summary>
        void timerAutoPurgelogs_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = globalCultureInfo;
                timerAutoPurgeLogs.AutoReset = false;
                GetSitePurgeLogDuartion();
                PurgeLogs();
                System.Threading.Thread.Sleep(5000);
                timerAutoPurgeLogs.AutoReset = true; ;
            }
            catch (Exception ex)
            {
            }
        }

        #endregion

        #region PurgeLogs
        /// <summary>
        /// PurgeLogs
        /// </summary>
        private void PurgeLogs()
        {
            string[] Files = null;
            try
            {
                MyVRMServer_ConfigPath = Path.GetDirectoryName(filePath);
                if (Directory.Exists(MyVRMServer_ConfigPath)) 
                {
                    Files = Directory.GetFiles(MyVRMServer_ConfigPath);
                    for (int i = 0; i < Files.Length; i++)
                    {
                        FileInfo fi = new FileInfo(Files[i]);
                        if (DateTime.UtcNow - fi.CreationTimeUtc > TimeSpan.FromDays(purgeDuration))
                            fi.Delete();
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        #endregion

        #region Retrieve Log Duration
        /// <summary>
        /// GetSitePurgeLogDuartion()
        /// </summary>
        public void GetSitePurgeLogDuartion()
        {
            string inXML = ""; var outXML = "";
            XmlDocument xdoc = new XmlDocument();
            try
            {
                inXML = "<myVRM><login>"+ myVRMUsername +"</login><password>"+ myVRMPassword +"</password><version>"+ myVRMVersion +"</version><client>"+ myVRMClient +"</client><commandname>GetSuperAdmin</commandname><command><login><userID>11</userID></login></command></myVRM>";
                outXML = WebrequestCall(inXML);

                if (outXML != "")
                {
                    xdoc.LoadXml(outXML);
                    XmlNodeList list = xdoc.GetElementsByTagName("AutoPurgeLogDuration");
                    string purgeDur = list[0].InnerText;
                    purgeDuration = int.Parse(purgeDur);
                }
            }
            catch (System.Exception ex)
            {
            }
        }

        #endregion

        #region WebrequestCall
        /// <summary>
        /// WebrequestCall
        /// </summary>
        private string WebrequestCall(string inXML)
        {
            string outXML = "";
            try
            {
                myVRMEWS.myVRMReference.VRMNewWebService service = new myVRMEWS.myVRMReference.VRMNewWebService();
                service.Url = myVRMURL;
                if (SSOMode.ToLower() == "true")
                    service.UseDefaultCredentials = true;
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                    ((sender1, certificate, chain, sslPolicyErrors) => true);
                outXML = service.InvokeWebservice(inXML);

            }
            catch (System.Exception ex)
            {
            }
            return outXML;
        }
        #endregion
        //ZD 104846 End
    }
}
