//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
namespace NS_LOGGER
{
    #region References
    using System;
    using System.IO;
    using System.Threading;
    using System.Diagnostics;
    #endregion

    class Log
    {
        private static string logFilePath = "C:\\VRMSchemas_v18\\RTCLog.log";

        public Log(String filePath)
        {
            logFilePath = filePath;

        }

        public void Trace(string message)
        {

            StackFrame CallStack = new StackFrame(1, true);
            string file = CallStack.GetFileName();
            string method = CallStack.GetMethod().Name;
            int line = CallStack.GetFileLineNumber();

            WriteToLogFile(message);
        }

        private void WriteToLogFile(string logRecord)
        {

            Console.WriteLine(logRecord);
            StreamWriter sw;
            String newLogfilepath = "";

            try
            {
                string sYear = DateTime.Now.Year.ToString();
                string sMonth = DateTime.Now.Month.ToString();
                string sDay = DateTime.Now.Day.ToString();

                string lgName = sYear + sMonth + sDay;

                newLogfilepath = logFilePath;

                newLogfilepath = newLogfilepath + "_" + lgName + ".log";

                if (!File.Exists(newLogfilepath))
                {
                    // file doesnot exist . hence, create a new log file.
                    sw = File.CreateText(newLogfilepath);
                    sw.Flush();
                    sw.Close();
                }
                else
                {
                    // check if exisiting log file size is greater than 50 MB
                    FileInfo fi = new FileInfo(newLogfilepath);
                    if (fi.Length > 50000000)
                    {
                        // delete the log file						
                        File.Delete(logFilePath);

                        // create a new log file 
                        sw = File.CreateText(newLogfilepath);
                        sw.Flush();
                        sw.Close();
                    }
                }

                // write the log record.
                sw = File.AppendText(newLogfilepath);
                sw.WriteLine(logRecord);
                sw.Flush();
                sw.Close();
            }
            catch (Exception)
            {
                // do nothing
            }
        }

        private string ReplaceInvalidChars(string input)
        {
            input = input.Replace("<", "&lt;");
            input = input.Replace(">", "&gt;");
            input = input.Replace("&", "&amp;");
            return (input);
        }

    }
}